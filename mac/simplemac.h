#ifndef _SIMPLEMAC_H_
#define _SIMPLEMAC_H_

// EUID64 for long MAC address
// uint8_t for short logic address
// uint8_t for service port

uint8_t start_csma_ca(uint8_t timeout, uint8_t retry);

typedef struct{
    uint32_t timeout;
    uint32_t mac_address;
    uint32_t address;
    uint32_t port;
    volatile uint8_t buffer;
    uint8_t crc
}TLV_TypeDef; 

#endif
