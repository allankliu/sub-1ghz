/*
 * refer to the nRF24.h
 * or its derivate at 
 * https://github.com/PaulStoffregen/RadioHead/blob/master/RH_NRF24.h
 *
 * check out RadioHead Packet Radio from 
 * https://www.airspayce.com/mikem/arduino/RadioHead/
 * https://www.airspayce.com/mikem/arduino/RadioHead/classRH__RF24.html
 * https://github.com/adafruit/RadioHead
 */

#ifndef __RADIO_H__
#define __RADIO_H__

#include <stdint.h>

#ifdef NRF24
// This is the maximum number of bytes that can be carried by the nRF24.
// We use some for headers, keeping fewer for RadioHead messages
//#define RH_NRF24_MAX_PAYLOAD_LEN 32
#define CMT2300_MAX_PAYLOAD_LEN 64

// The length of the headers we add.
// The headers are inside the nRF24 payload
//#define RH_NRF24_HEADER_LEN 4
#define CMT2300_HEADER_LEN 4

// This is the maximum RadioHead user message length that can be supported by this library. Limited by
// the supported message lengths in the nRF24
//#define RH_NRF24_MAX_MESSAGE_LEN (RH_NRF24_MAX_PAYLOAD_LEN-RH_NRF24_HEADER_LEN)
#define CMT2300_MAX_MESSAGE_LEN (CMT2300_MAX_PAYLOAD_LEN - CMT2300_HEADER_LEN)

// SPI Command names
//#define RH_NRF24_COMMAND_R_REGISTER                        0x00
//#define RH_NRF24_COMMAND_W_REGISTER                        0x20
//#define RH_NRF24_COMMAND_ACTIVATE                          0x50 // only on RFM73 ?
//#define RH_NRF24_COMMAND_R_RX_PAYLOAD                      0x61
//#define RH_NRF24_COMMAND_W_TX_PAYLOAD                      0xa0
//#define RH_NRF24_COMMAND_FLUSH_TX                          0xe1
//#define RH_NRF24_COMMAND_FLUSH_RX                          0xe2
//#define RH_NRF24_COMMAND_REUSE_TX_PL                       0xe3
//#define RH_NRF24_COMMAND_R_RX_PL_WID                       0x60
//#define RH_NRF24_COMMAND_W_ACK_PAYLOAD(pipe)               (0xa8|(pipe&0x7))
//#define RH_NRF24_COMMAND_W_TX_PAYLOAD_NOACK                0xb0
//#define RH_NRF24_COMMAND_NOP                               0xff

#define CMT2300_COMMAND_SOFT_RESET                         0x00
#define CMT2300_COMMAND_GO_SLEEP                           0x01
#define CMT2300_COMMAND_GO_STANDBY                         0x02
#define CMT2300_COMMAND_GO_RFS                             0x03
#define CMT2300_COMMAND_GO_TFS                             0x04
#define CMT2300_COMMAND_GO_RX                              0x05
#define CMT2300_COMMAND_GO_TX                              0x06

// Register names
#define CMT2300_REGISTER_MASK               0x7F
#define CMT2300_REG_0C_SYS1                 0x0C
#define CMT2300_REG_0D_SYS2                 0x0D
#define CMT2300_REG_0E_SYS3                 0x0E
#define CMT2300_REG_0F_SYS4                 0x0F
#define CMT2300_REG_10_SYS5                 0x10
#define CMT2300_REG_11_SYS6                 0x11
#define CMT2300_REG_12_SYS7                 0x12
#define CMT2300_REG_13_SYS8                 0x13
#define CMT2300_REG_14_SYS9                 0x14
#define CMT2300_REG_15_SYS10                0x15
#define CMT2300_REG_16_SYS11                0x16
#define CMT2300_REG_17_SYS12                0x17

#define CMT2300_REG_38_PKT1                 0x38
#define CMT2300_REG_39_PKT2                 0x39
#define CMT2300_REG_3A_PKT3                 0x3A
#define CMT2300_REG_3B_PKT4                 0x3B
#define CMT2300_REG_3C_PKT5                 0x3C
#define CMT2300_REG_3D_PKT6                 0x3D
#define CMT2300_REG_3E_PKT7                 0x3E
#define CMT2300_REG_3F_PKT8                 0x3F
#define CMT2300_REG_40_PKT9                 0x40
#define CMT2300_REG_41_PKT10                0x41
#define CMT2300_REG_42_PKT11                0x42
#define CMT2300_REG_43_PKT12                0x43
#define CMT2300_REG_44_PKT13                0x44
#define CMT2300_REG_45_PKT14                0x45
#define CMT2300_REG_46_PKT15                0x46
#define CMT2300_REG_47_PKT16                0x47
#define CMT2300_REG_48_PKT17                0x48
#define CMT2300_REG_49_PKT18                0x49
#define CMT2300_REG_4A_PKT19                0x4A
#define CMT2300_REG_4B_PKT20                0x4B
#define CMT2300_REG_4C_PKT21                0x4C
#define CMT2300_REG_4D_PKT22                0x4D
#define CMT2300_REG_4E_PKT23                0x4E
#define CMT2300_REG_4F_PKT24                0x4F
#define CMT2300_REG_50_PKT25                0x50
#define CMT2300_REG_51_PKT26                0x51
#define CMT2300_REG_52_PKT27                0x52
#define CMT2300_REG_53_PKT28                0x53
#define CMT2300_REG_54_PKT29                0x54

#define CMT2300_REG_60_MODE_CTL             0x60
#define CMT2300_REG_61_MODE_STA             0x61
#define CMT2300_REG_62_EN_CTL               0x62
#define CMT2300_REG_63_FREQ_CHNL            0x63
#define CMT2300_REG_64_FREQ_OFNS            0x64
#define CMT2300_REG_65_IO_SEL               0x65
#define CMT2300_REG_66_INT1_CTL             0x66
#define CMT2300_REG_67_INT2_CTL             0x67
#define CMT2300_REG_68_INT_EN               0x68
#define CMT2300_REG_69_FIFO_CTL             0x69
#define CMT2300_REG_6A_INT_CLR1             0x6A
#define CMT2300_REG_6B_INT_CLR2             0x6B
#define CMT2300_REG_6C_FIFO_CLR             0x6C
#define CMT2300_REG_6D_INT_FLAG             0x6D
#define CMT2300_REG_6E_FIFO_FLAG            0x6E
#define CMT2300_REG_6F_RSSI_CODE            0x6F
#define CMT2300_REG_70_RSSI_DBM             0x70
#define CMT2300_REG_71_LBD_RESULT           0x71

// Register names
//#define RH_NRF24_REGISTER_MASK                             0x1f
//#define RH_NRF24_REG_00_CONFIG                             0x00
//#define RH_NRF24_REG_01_EN_AA                              0x01
//#define RH_NRF24_REG_02_EN_RXADDR                          0x02
//#define RH_NRF24_REG_03_SETUP_AW                           0x03
//#define RH_NRF24_REG_04_SETUP_RETR                         0x04
//#define RH_NRF24_REG_05_RF_CH                              0x05
//#define RH_NRF24_REG_06_RF_SETUP                           0x06
//#define RH_NRF24_REG_07_STATUS                             0x07
//#define RH_NRF24_REG_08_OBSERVE_TX                         0x08
//#define RH_NRF24_REG_09_RPD                                0x09
//#define RH_NRF24_REG_0A_RX_ADDR_P0                         0x0a
//#define RH_NRF24_REG_0B_RX_ADDR_P1                         0x0b
//#define RH_NRF24_REG_0C_RX_ADDR_P2                         0x0c
//#define RH_NRF24_REG_0D_RX_ADDR_P3                         0x0d
//#define RH_NRF24_REG_0E_RX_ADDR_P4                         0x0e
//#define RH_NRF24_REG_0F_RX_ADDR_P5                         0x0f
//#define RH_NRF24_REG_10_TX_ADDR                            0x10
//#define RH_NRF24_REG_11_RX_PW_P0                           0x11
//#define RH_NRF24_REG_12_RX_PW_P1                           0x12
//#define RH_NRF24_REG_13_RX_PW_P2                           0x13
//#define RH_NRF24_REG_14_RX_PW_P3                           0x14
//#define RH_NRF24_REG_15_RX_PW_P4                           0x15
//#define RH_NRF24_REG_16_RX_PW_P5                           0x16
//#define RH_NRF24_REG_17_FIFO_STATUS                        0x17
//#define RH_NRF24_REG_1C_DYNPD                              0x1c
//#define RH_NRF24_REG_1D_FEATURE                            0x1d

// These register masks etc are named wherever possible
// corresponding to the bit and field names in the nRF24L01 Product Specification
// #define RH_NRF24_REG_00_CONFIG                             0x00
#define RH_NRF24_MASK_RX_DR                                0x40
#define RH_NRF24_MASK_TX_DS                                0x20
#define RH_NRF24_MASK_MAX_RT                               0x10
#define RH_NRF24_EN_CRC                                    0x08
#define RH_NRF24_CRCO                                      0x04
#define RH_NRF24_PWR_UP                                    0x02
#define RH_NRF24_PRIM_RX                                   0x01

// #define RH_NRF24_REG_01_EN_AA                              0x01
#define RH_NRF24_ENAA_P5                                   0x20
#define RH_NRF24_ENAA_P4                                   0x10
#define RH_NRF24_ENAA_P3                                   0x08
#define RH_NRF24_ENAA_P2                                   0x04
#define RH_NRF24_ENAA_P1                                   0x02
#define RH_NRF24_ENAA_P0                                   0x01

// #define RH_NRF24_REG_02_EN_RXADDR                          0x02
#define RH_NRF24_ERX_P5                                    0x20
#define RH_NRF24_ERX_P4                                    0x10
#define RH_NRF24_ERX_P3                                    0x08
#define RH_NRF24_ERX_P2                                    0x04
#define RH_NRF24_ERX_P1                                    0x02
#define RH_NRF24_ERX_P0                                    0x01

// #define RH_NRF24_REG_03_SETUP_AW                           0x03
#define RH_NRF24_AW_3_BYTES                                0x01
#define RH_NRF24_AW_4_BYTES                                0x02
#define RH_NRF24_AW_5_BYTES                                0x03

// #define RH_NRF24_REG_04_SETUP_RETR                         0x04
#define RH_NRF24_ARD                                       0xf0
#define RH_NRF24_ARC                                       0x0f

// #define RH_NRF24_REG_05_RF_CH                              0x05
#define RH_NRF24_RF_CH                                     0x7f

// #define RH_NRF24_REG_06_RF_SETUP                           0x06
#define RH_NRF24_CONT_WAVE                                 0x80
#define RH_NRF24_RF_DR_LOW                                 0x20
#define RH_NRF24_PLL_LOCK                                  0x10
#define RH_NRF24_RF_DR_HIGH                                0x08
#define RH_NRF24_PWR                                       0x06
#define RH_NRF24_PWR_m18dBm                                0x00
#define RH_NRF24_PWR_m12dBm                                0x02
#define RH_NRF24_PWR_m6dBm                                 0x04
#define RH_NRF24_PWR_0dBm                                  0x06
#define RH_NRF24_LNA_HCURR                                 0x01

// #define RH_NRF24_REG_07_STATUS                             0x07
#define RH_NRF24_RX_DR                                     0x40
#define RH_NRF24_TX_DS                                     0x20
#define RH_NRF24_MAX_RT                                    0x10
#define RH_NRF24_RX_P_NO                                   0x0e
#define RH_NRF24_STATUS_TX_FULL                            0x01

// #define RH_NRF24_REG_08_OBSERVE_TX                         0x08
#define RH_NRF24_PLOS_CNT                                  0xf0
#define RH_NRF24_ARC_CNT                                   0x0f

// #define RH_NRF24_REG_09_RPD                                0x09
#define RH_NRF24_RPD                                       0x01

// #define RH_NRF24_REG_17_FIFO_STATUS                        0x17
#define RH_NRF24_TX_REUSE                                  0x40
#define RH_NRF24_TX_FULL                                   0x20
#define RH_NRF24_TX_EMPTY                                  0x10
#define RH_NRF24_RX_FULL                                   0x02
#define RH_NRF24_RX_EMPTY                                  0x01

// #define RH_NRF24_REG_1C_DYNPD                              0x1c
#define RH_NRF24_DPL_ALL                                   0x3f
#define RH_NRF24_DPL_P5                                    0x20
#define RH_NRF24_DPL_P4                                    0x10
#define RH_NRF24_DPL_P3                                    0x08
#define RH_NRF24_DPL_P2                                    0x04
#define RH_NRF24_DPL_P1                                    0x02
#define RH_NRF24_DPL_P0                                    0x01

// #define RH_NRF24_REG_1D_FEATURE                            0x1d
#define RH_NRF24_EN_DPL                                    0x04
#define RH_NRF24_EN_ACK_PAY                                0x02
#define RH_NRF24_EN_DYN_ACK                                0x01
#endif

typedef enum{
    RX_ACTIVE = 0,
    TX_ACTIVE = 1,
    RSS_VLD,
    PREAM_OK,
    SYNC_OK,
    NODE_OK,
    CRC_OK,
    PKT_OK,
    SL_TMO,
    RX_TMO,
    TX_DONE,
    RX_FIFO_NMTY,
    RX_FIFO_TH,
    RX_FIFO_FULL,
    RX_FIFO_WBYTE,
    RX_FIFO_OVF,
    TX_FIFO_NMTY,
    TX_FIFO_TH,
    TX_FIFO_FULL,
    STATE_IS_STBY,
    STATE_IS_FS,
    STATE_IS_RX,
    STATE_IS_TX,
    LBD,
    TRX_ACTIVE,
    PKT_DONE
}RADIO_INT;

typedef enum{
    RADIO_IDLE = 0,
    RADIO_SLEEP,
    RADIO_STANDBY,
    RADIO_RFS,
    RADIO_TFS,
    RADIO_RX,
    RADIO_TX
}RADIO_MODE;

typedef enum{
    RADIO_DR_300k = 0,
    RADIO_DR_200k,
    RADIO_DR_100k,
    RADIO_DR_50k,
    RADIO_DR_20k,
    RADIO_DR_9k6,
    RADIO_DR_2k4,
    RADIO_DR_1k2
}RADIO_DATA_RATE;

typedef enum{
    RADIO_TXPWR_20dBm = 0,
    RADIO_TXPWR_19dBm,
    RADIO_TXPWR_18dBm,
    RADIO_TXPWR_17dBm,
    RADIO_TXPWR_16dBm,
    RADIO_TXPWR_15dBm,
    RADIO_TXPWR_14dBm,
    RADIO_TXPWR_13dBm,
    RADIO_TXPWR_12dBm,
    RADIO_TXPWR_11dBm,
    RADIO_TXPWR_10dBm,
    RADIO_TXPWR_9dBm,
    RADIO_TXPWR_8dBm,
    RADIO_TXPWR_7dBm,
    RADIO_TXPWR_6dBm,
    RADIO_TXPWR_5dBm,
    RADIO_TXPWR_4dBm,
    RADIO_TXPWR_3dBm,
    RADIO_TXPWR_2dBm,
    RADIO_TXPWR_1dBm,
    RADIO_TXPWR_0dBm    
}RADIO_TX_POWER;

boolean init(void);
uint8_t spiReadRegister(uint8_t reg);
uint8_t spiWriteRegister(uint8_t reg, uint8_t value);
uint8_t spiReadBurstRegister(uint8_t reg, unit8_t* dest, unit8_t len);
uint8_t spiWriteBurstRegister(uint8_t reg, uint8_t* src, unit8_t len);
uint8_t spiCommand(uint8_t cmd);
uint8_t spiRead(uint8_t cmd);
uint8_t spiWrite(uint8_t cmd, uint8_t value);


uint8_t statusRead(void);
uint8_t flushTx(void);
uint8_t flushRx(void);
boolean setChannel(uint8_t channel);
boolean setFrequency(uint32_t frequency);
boolean setOpMode(unit8_t mode);
boolean setNwkAddress(unit8_t* nwkAddress, unit8_t len);
boolean setRF(RADIO_DATA_RATE datarate, RADIO_TX_POWER, txpwr);
void setModeIdle(void);
void setModeRx(void);
void setModeTx(void);
void setTxPower(uint8_t power);
boolean send(const uint8_t* data, uint8_t len);
boolean isSending(void);
boolean printRegisters(void);
boolean available(void);
boolean recv(uint8_t* data, uint8_t len);
uint8_t maxMessageSize(void);
void sleep(void);
void validateRxBuf(void);
void clearRxBuf(void);

#endif
