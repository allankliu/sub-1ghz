#include "radio.h"
#include "spi.h"
#include "string.h"

static uint8_t txBuffer[64];
static uint8_t rxBuffer[64];
static uint8_t txBufferLen;
static uint8_t rxBufferLen;

static uint8_t channel;
static uint8_t eui64[8];
static uint8_t address;


boolean init(void){
    spiWriteRegister();
}

void handleInterrupt(void){
    // read int register and checkout int sources
    // invalid preamble
    // invalid syncword
    // crc error
    // packet sent
    // packet received
    // fifo empty
    // fifo full
}

uint8_t spiReadRegister(uint8_t reg){}
uint8_t spiWriteRegister(uint8_t reg, uint8_t value){}
uint8_t spiReadBurstRegister(uint8_t reg, unit8_t* dest, unit8_t len){}
uint8_t spiWriteBurstRegister(uint8_t reg, uint8_t* src, unit8_t len){}
uint8_t spiCommand(uint8_t cmd){}
uint8_t spiRead(uint8_t cmd){}
uint8_t spiWrite(uint8_t cmd, uint8_t value){}


uint8_t statusRead(void){
    return spiRead(CMT2300_REG_61_MODE_STA);
}

uint8_t flushTx(void){}
uint8_t flushRx(void){}
boolean setChannel(uint8_t channel){}
boolean setFrequency(uint32_t frequency){

}

boolean setOpMode(unit8_t mode){}
boolean setNwkAddress(unit8_t* nwkAddress, unit8_t len){}
boolean setRF(RADIO_DATA_RATE datarate, RADIO_TX_POWER txpwr){}

void setModeIdle(void){
    spiCommand(CMT2300_REG_60_MODE_CTL, CMT2300_COMMAND_SOFT_RESET);
}

void setModeRx(void){
    spiCommand(CMT2300_REG_60_MODE_CTL, CMT2300_COMMAND_GO_RX);
}

void setModeTx(void){
    spiCommand(CMT2300_REG_60_MODE_CTL, CMT2300_COMMAND_GO_TX);
}

void setTxPower(uint8_t power){
    spiWrite(CMT2300_REG_TX_POWER, power);
}

boolean send(const uint8_t* data, uint8_t len){}
boolean isSending(void){}
boolean printRegisters(void){}

boolean available(void){

}

boolean recv(uint8_t* data, uint8_t len){
    memcpy(data, rxBuffer, len);
    memset(rcBuffer, 0, len);
}

uint8_t maxMessageSize(void){
    return CMT2300_MAX_PAYLOAD_LEN;
}

void sleep(void){
    spiCommand(CMT2300_REG_60_MODE_CTL, CMT2300_COMMAND_GO_SLEEP);
}

void validateRxBuf(void){}

void clearRxBuf(void){
    memset(rxBuf, 0, sizeof(rxBuf));
}
