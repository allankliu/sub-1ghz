#ifndef _RUNTIME_PARAM_
#define _RUNTIME_PARAM_

#include "stdint.h"

typedef struct {
    uint32_t carrier;
    uint16_t bandwidth;
    uint8_t channel;
    uint8_t modulation;
    uint16_t symbolrate;
    uint8_t short_address;
    uint8_t mac_address[8];
    uint8_t ipv6_address[128];
    uint8_t router_address[128];
    uint8_t neighbour_address[8];
    uint8_t packet_ttl;
}STRUCT_RT_PARAM;

#endif
