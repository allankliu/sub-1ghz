## Components

- CMT2300A,
- HW3000,

```
PART    CMT2300A    HW3000
FREQ    127~1020    315~915
DATA    0.5~300k    1.2~100k
BW      50~500kHz   10~200kHz
MODEM   GFSK/GMSK   GFSK
SENSE   -121dBm@2k  -114dBm@10k
VOLT    1.8~3.6V    2~3.6V
TXPWR   72mA@20dBm  90mA@20dBm
RXPWR   8.5mA       8mA
LPM     300nA       100nA
FIFO    64B         256B
MOQ     3K
Extra               IEEE802.15.4g
Pack    QFN16       QFN20
LINK    141dBm      134dBm
```

This is the part of second source project for IEEE802.15.4 in China, as the alternative silicons for TI/Chipcom/Silicon Labs/Atmel/NXP/Microchip platforms.

According to the result of datasheets, we can conclude that CMT2300A from HopeRF/CMOSTek seems better than HW3000 from EastSoft/essemi, with more options on RF carrier technologies, smaller package, longer distance and lower power consumption. However HW3000 has IEEE802.15.4g support and longer FIFO. 